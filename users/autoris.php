<?php
session_start();
require_once ($_SERVER['DOCUMENT_ROOT'].'/init.php');

if (isset($_SESSION['user']['role'])) {         //Попытки авторизованного пользователя зайти на страницу авторизации, должны переадресовывать пользователя в его личный кабинет.
    $redirect_url='http://shop.tokarniy-stanok.com.ua/users/' . $_SESSION['user']['role'] . '.php';
    echo "<script>window.location.href='$redirect_url';</script>";
}
?>
   

    <p><?php echo $dictionary['enter_m_p']; ?></p>
    <form method="post">
        <p>email: <input type="text" name="email"><br>
            <br><?php echo $dictionary['password']; ?><input type="text" name="pass">
        <p><input type="submit" name="" value="Войти"></p>
    </form>

<?php

if (isset($_POST["email"]) and isset($_POST["pass"])) {      //проверяем чтоб данные были, иначе выдает ошибку
    $email = $_POST["email"];
    $password = $_POST["pass"];

    foreach ($users as $key => $value) {
        if (($value['email'] == $email) and ($value['password'] == $password)) {
            $_SESSION['user'] = $value;
            $_SESSION['user']['id'] = $key;
            $user = new User();                    //создаем юзера
            switch ($value['role']) {             //создаем потомка согласно роли, $redirect_url для переадресации в личный кабинет
                case 'client':
                    $client = new Client();
                    $redirect_url = "client.php";
                    break;
                case 'manager':
                    $manager = new Manager();
                    $redirect_url = "manager.php";
                    break;
                case 'admin':
                    $admin = new Admin();
                    $redirect_url = "admin.php";
                    break;
            }
            echo "<script>self.location='$redirect_url';</script>";         //переадресация
            break;
        }
    }
    if (!isset($_SESSION['user']['id'])) {
        echo $dictionary['no_user'];
    }
} else {
    echo $dictionary['fill']."<br>";
}
var_dump('<pre>', $users);
