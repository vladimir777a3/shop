<?php
session_start();

if ($_SESSION["user"]["role"] != 'admin')                   //Если прав нет, то показываем ему ошибку 403. <?php header('HTTP/1.0 403 Forbidden');
{
    header("HTTP/1.0 403 Forbidden");
    exit();
}

require_once($_SERVER['DOCUMENT_ROOT'] . '/init.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/header.php');

?>

<h1>Личный кабинет Админа</h1>
<p>Секретная информация</p>
<a href="client.php"><?php echo $dictionary['clients_account']; ?></a><br>
<a href="manager.php"><?php echo $dictionary['managers_account']; ?></a><br>

<a href="http://shop.tokarniy-stanok.com.ua/exit.php"><?php echo $dictionary['exit']; ?></a><br>

