<?php
session_start();
require_once ($_SERVER['DOCUMENT_ROOT'].'/init.php');
require_once ($_SERVER['DOCUMENT_ROOT'].'/header.php');

/*В файле содержится форма и вызов функции добавления товара в корзину.
В форме: выбор товаров из выпадающего списка select, ввод количества товара.
при отправке формы добавляем товар в корзину в сессию с помощью вызова функции из файла cart.php*/

?>
<p><?php echo $dictionary['what_product']; ?></p>
<form method="get">
    <?php echo $dictionary['product']; ?><select name="new_id">
            <option selected disabled><?php echo $dictionary['select_product']; ?></option>
            <?php
                foreach ($products as $key => $value) {
                    echo "<option value=\"".$key."\">".$value['name']."</option>";
                }
            ?>
        </select></p>
    <br><?php echo $dictionary['number']; ?>: <input type="text" name="new_quantity">
    <p><input type="submit" name="" value="<?php echo $dictionary['add_to_cart']; ?>"></p>

</form>

<?php
$object_cart->add_to_cart();
$object_cart->recount_cart();
echo "<br><br>";
require_once ($_SERVER['DOCUMENT_ROOT'].'/cart/index.php');
?>