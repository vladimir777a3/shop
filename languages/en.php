++подключен en.php++

<?php

$dictionary = [
    'hi'=>'Hello',
    'client'=>'client',
    'manager'=>'manager',
    'admin'=>'admin',
    'client_can'=>'You can view the information available to users on the site',
    'manager_can'=>'You can change, delete and create clients on the site',
    'admin_can'=>'You can do everything on the site',
    'main'=>'The main',
    'cart'=>'Cart',
    'account'=>'Personal Area',
    'exit'=>'Sign out of your account',
    'enter_m_p'=>'Enter your email and password:',
    'password'=>'Password: ',
    'enter'=>'To come in',//надпись на кнопке авторизации
    'fill'=>'Please fill in all fields',
    'no_user'=>'Error: User with such data was not found',
    'managers_account'=>'Manager\'s personal account',
    'clients_account'=>'Client\'s personal account',
    'add_products'=>'Add more products',
    'cart_empty'=>'Cart is empty',
    'add_to_cart'=>'Add to Shopping Cart',//надпись на кнопке
    'what_product'=>'What product are we adding to the cart? Select a product and indicate its quantity:',
    'product'=>'Product: ',
    'select_product'=>'Select a product',
    'number'=>'Number',
    'not_selected'=>'Product not selected or quantity not entered',
    'incorrect_quantity'=>'The quantity is incorrect or the product is not selected correctly',
    'name'=>'Name',
    'price'=>'Price',
    'total'=>'Total',
    'delete'=>'delete',
    'total_amount'=>'Total amount: ',
    'with_discount'=>'To pay with a discount: ',
    'lang'=>'Language: ',
    'lang_push'=>'Apply language',
];

