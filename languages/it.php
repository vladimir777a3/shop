++подключен it.php++

<?php

$dictionary = [
    'hi'=>'Ciao',
    'client'=>'cliente',
    'manager'=>'manager',
    'admin'=>'admin',
    'client_can'=>'È possibile visualizzare le informazioni disponibili per gli utenti sul sito',
    'manager_can'=>'Puoi modificare, eliminare e creare client sul sito',
    'admin_can'=>'Puoi fare tutto sul sito',
    'main'=>'Il principale',
    'cart'=>'Cestino',
    'account'=>'Area personale',
    'exit'=>'Esci dal tuo account',
    'enter_m_p'=>'Inserisci la tua email e la tua password:',
    'password'=>'Parola d\'ordine: ',
    'enter'=>'Entrare',//надпись на кнопке авторизации
    'fill'=>'Si prega di compilare tutti i campi',
    'no_user'=>'Errore: l\'utente con questi dati non è stato trovato',
    'managers_account'=>'Account personale del manager',
    'clients_account'=>'Account personale del cliente',
    'add_products'=>'Aggiungi altri prodotti',
    'cart_empty'=>'Il carrello è vuoto',
    'add_to_cart'=>'Aggiungere al carrello della spesa',//надпись на кнопке
    'what_product'=>'Quale prodotto stiamo aggiungendo al carrello? Seleziona un prodotto e indica la sua quantità:',
    'product'=>'Prodotto: ',
    'select_product'=>'Seleziona un prodotto',
    'number'=>'Numero',
    'not_selected'=>'Prodotto non selezionato o quantità non inserita',
    'incorrect_quantity'=>'La quantità non è corretta o il prodotto non è selezionato correttamente',
    'name'=>'Nome',
    'price'=>'Prezzo',
    'total'=>'Quantità',
    'delete'=>'Elimina',
    'total_amount'=>'Importo totale: ',
    'with_discount'=>'Da pagare con uno sconto: ',
    'lang'=>'linguaggio: ',
    'lang_push'=>'Applica la lingua',
];

